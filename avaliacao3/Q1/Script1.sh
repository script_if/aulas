#!/bin/bash

if [ -z "$1" ]; then
  echo "Uso: $0 <nome_de_usuário>"
  exit 1
fi

u=$1

mostrar_menu() {
  echo "1 -> Verifica se o usuário existe"
  echo "2 -> Verifica se o usuário está logado na máquina"
  echo "3 -> Lista os arquivos da pasta home do usuário"
  echo "4 -> Sair"
  echo -n "Escolha uma opção: "
}

while true; do
  mostrar_menu
  read op

  case $op in
    1)
      if id "$u" &>/dev/null; then
        echo "O usuário '$u' existe."
      else
        echo "O usuário '$u' não existe."
      fi
      ;;
    2)
      if ! id "$u" &>/dev/null; then
        echo "Erro: O usuário '$u' não existe."
        exit 1
      fi
      if who | grep -w "$u" &>/dev/null; then
        echo "O usuário '$u' está logado."
      else
        echo "O usuário '$u' não está logado."
      fi
      ;;
    3)
      if ! id "$u" &>/dev/null; then
        echo "Erro: O usuário '$u' não existe."
        exit 1
      fi
      if [ -d "/home/$u" ]; then
        ls "/home/$u"
      else
        echo "A pasta home de $u não foi encontrada."
      fi
      ;;
    4)
      echo "Saindo..."
      exit 0
      ;;
    *)
      echo "Opção inválida!"
      ;;
  esac
done
