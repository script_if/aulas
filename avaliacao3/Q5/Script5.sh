#!/bin/bash

if [ $# -ne 1 ]; then
  echo "Uso: $0 <senha>"
  exit 1
fi

s=$1

if [[ $s =~ [A-Z] ]] && [[ $s =~ [a-z] ]] && [[ $s =~ [0-9] ]]; then
  echo "Senha válida!"
else
  echo "Senha inválida! A senha deve conter pelo menos uma letra maiúscula, uma letra minúscula e um número."
fi
