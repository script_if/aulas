#!/bin/bash

clear

mostrar_uso() {
  echo "Uso: $0 [-d | -e | -s] <pasta>"
  echo "  -d: Lista apenas diretórios"
  echo "  -e: Lista apenas executáveis"
  echo "  -s: Lista apenas scripts Shell"
  exit 1
}

if [ $# -ne 2 ]; then
  mostrar_uso
fi

p=$2
if [ ! -d "$p" ]; then
  echo "Erro: '$p' não é um diretório válido."
  exit 1
fi

case $1 in
  -d)
    echo "Listando diretórios em '$p':"
    ls -l "$p" | grep '^d'
    ;;
  -e)
    echo "Listando executáveis em '$p':"
    ls -l "$p" | grep '^-..x'
    ;;
  -s)
    echo "Listando scripts Shell em '$p':"
    ls "$p" | grep '\.sh$'
    ;;
  *)
    mostrar_uso
    ;;
esac
