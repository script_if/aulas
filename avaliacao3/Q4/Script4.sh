#!/bin/bash

if [ $# -ne 1 ]; then
  echo "Uso: $0 <arquivo_texto>"
  exit 1
fi

a=$1

if [ ! -f "$a" ]; then
  echo "Erro: '$a' não é um arquivo válido."
  exit 1
fi

grep -o 'R\$ [0-9]\{1,3\}\(\.[0-9]\{3\}\)*,[0-9]\{2\}' "$a"
