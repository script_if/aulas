#!/bin/bash

if [ $# -ne 2 ]; then
  echo "Uso: $0 [-r | -c] <arquivo>"
  echo "  -r: Remove as linhas em branco"
  echo "  -c: Conta as linhas em branco"
  exit 1
fi

a=$2

if [ ! -f "$a" ]; then
  echo "Erro: '$a' não é um arquivo válido."
  exit 1
fi

case $1 in
  -r)
    grep -v '^$' "$a" > temp_file && mv temp_file "$a"
    echo "Linhas em branco removidas de '$a'."
    ;;
  -c)
    count=$(grep -c '^$' "$a")
    echo "O arquivo '$a' contém $count linha(s) em branco."
    ;;
  *)
    echo "Opção inválida!"
    exit 1
    ;;
esac
