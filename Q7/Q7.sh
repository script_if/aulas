#!/bin/bash

a=$(date +%s)

b=$(( 60 * 60 * 24 ))

c=$(( a + (3 - $(date +%w)) * b ))

d=$(( c + 7 * b ))

date -d @$c "+Próxima quarta-feira: %d-%m-%Y"
date -d @$d "+Próxima próxima quarta-feira: %d-%m-%Y"
