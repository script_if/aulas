#!/bin/bash

echo -e "\e[31mO amor é fogo que arde sem se ver\e[0m"
sleep 1.5
echo -e "\e[32mÉ ferida que dói e não se sente\e[0m"
sleep 2
echo -e "\e[33mÉ um contentamento descontente\e[0m"
sleep 0.8
echo -e "\e[34mÉ dor que desatina sem doer\e[0m"
sleep 1
echo -e "\e[35mÉ um não querer mais que bem querer\e[0m"
