#!/bin/bash

echo "Informações da CPU:"
lscpu

apt install lshw

echo "Informações detalhadas do hardware:"
lshw -short
