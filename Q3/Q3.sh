#!/bin/bash
a="Hello"
b=42

echo "Digite um valor para a variável:"
read c

echo "O valor de a é: $a"
echo "O valor de b é: $b"
echo "O valor digitado pelo usuário é: $c"

echo "Nome do script: $0"
echo "Número de argumentos: $#"
echo "Todos os argumentos passados: $@"
echo "Status de saída do último comando: $?"
