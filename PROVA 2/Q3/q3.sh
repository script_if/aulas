#!/bin/bash


if ! command -v yad &> /dev/null
then
    echo "O yad não está instalado. Instale-o para continuar."
    exit 1
fi


if ! command -v at &> /dev/null
then
    yad --error --text="O comando 'at' não está instalado. Por favor, instale-o para continuar."
    exit 1
fi

schedule_backup() {
    local datetime=$1
    echo "bash /home/$username/Prova/Q2/q2.sh" | at "$datetime"
    if [ $? -eq 0 ]; then
        yad --info --text="Backup agendado para $datetime."
    else
        yad --error --text="Erro ao agendar o backup."
    fi
}

datetime=$(yad --calendar --title="Selecionar Data" --text="Selecione a data para o agendamento")

if [ -n "$datetime" ]; then
    time=$(yad --entry --title="Selecionar Hora" --text="Digite a hora para o agendamento (HH:MM):" --entry-text="HH:MM")
    if [ -n "$time" ]; then
        # Formatar data e hora para o formato esperado pelo comando 'at'
        formatted_date=$(date -d "$datetime" +"%Y-%m-%d")
        formatted_time=$(date -d "$time" +"%H:%M")
        formatted_datetime="$formatted_time $formatted_date"
        schedule_backup "$formatted_datetime"
    else
        yad --error --text="Nenhuma hora inserida. Tente novamente."
    fi
else
    yad --error --text="Nenhuma data selecionada. Tente novamente."
fi
