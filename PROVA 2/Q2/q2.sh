#!/bin/bash

timestamp=$(date +"%H%M")
backup_dir="/tmp/backup"
echo "Criando diretório de backup: $backup_dir"
mkdir -p $backup_dir

backup_file="$backup_dir/backup_$timestamp.tar.gz"
echo "Compactando arquivos para: $backup_file"
tar -czf $backup_file -C /home/$USER .

if [ $? -eq 0 ]; then
    echo "Backup concluído: $backup_file"
else
    echo "Erro ao criar o backup"
    exit 1
fi

