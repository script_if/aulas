#!/bin/bash

a="/tmp"
b="/etc"
c="/bin"

echo "Quantidade de arquivos e diretórios em $a:"
ls -l $a | grep -v '^d' | wc -l
echo "Quantidade de diretórios em $a:"
ls -l $a | grep '^d' | wc -l

echo "Quantidade de arquivos e diretórios em $b:"
ls -l $b | grep -v '^d' | wc -l
echo "Quantidade de diretórios em $b:"
ls -l $b | grep '^d' | wc -l

echo "Quantidade de arquivos e diretórios em $c:"
ls -l $c | grep -v '^d' | wc -l
echo "Quantidade de diretórios em $c:"
ls -l $c | grep '^d' | wc -l
