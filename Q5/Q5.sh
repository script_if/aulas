#!/bin/bash

echo "Digite 4 nomes para os diretórios:"
read d e f g

mkdir "$d" "$e" "$f" "$g"

for h in "$d" "$e" "$f" "$g"; do
    echo -e "# $h\n\n$(date +'%d-%m-%Y')" > "$h/README.md"
done
