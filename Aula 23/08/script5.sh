#!/bin/bash

orig="abcdefghijklmnopqrstuvwxyz"
sub="zyxwvutsrqponmlkjihgfedcba"

decr() {
  txt=$1
  echo "$txt" | tr "$sub" "$orig"
}

echo "Digite o nome do arquivo que deseja descriptografar:"
read arq

if [ ! -f "$arq" ]; then
  echo "Arquivo não encontrado!"
  exit 1
fi

cont=$(cat "$arq")
cont_decr=$(decr "$cont")

echo "$cont_decr" > "$arq"
echo "Arquivo descriptografado com sucesso!"
