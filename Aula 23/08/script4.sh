#!/bin/bash

alfabeto_original="abcdefghijklmnopqrstuvwxyz"
alfabeto_substituicao="zyxwvutsrqponmlkjihgfedcba"

criptografar_texto() {
  texto=$1
  echo "$texto" | tr "$alfabeto_original" "$alfabeto_substituicao"
}

echo "Digite o nome do arquivo que deseja criptografar:"
read nome_arquivo

conteudo=$(cat "$nome_arquivo")
conteudo_criptografado=$(criptografar_texto "$conteudo")

echo "$conteudo_criptografado" > "$nome_arquivo"
echo "Arquivo criptografado com sucesso!"
