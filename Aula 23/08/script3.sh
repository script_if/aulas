#!/bin/bash

arq="dados.txt"
nome="dados_formatados.txt"

> "$nome"

while IFS= read -r l; do
    c=$(echo "$l" | tr -cd '0-9 /')
    f=$(echo "$c" | tr -s '[:space:]')
    echo "$f" >> "$nome"
done < "$arq"

