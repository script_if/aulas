#!/bin/bash

gerar() {
    local TAM=$1
    local MAI=$2
    local MIN=$3
    local NUM=$4
    local ESP=$5
    local ARQ_TMP="/tmp/carac_senha.txt"

    > $ARQ_TMP

    if [ "$MAI" == "TRUE" ]; then
        echo {A..Z} >> $ARQ_TMP
    fi

    if [ "$MIN" == "TRUE" ]; then
        echo {a..z} >> $ARQ_TMP
    fi

    if [ "$NUM" == "TRUE" ]; then
        echo {0..9} >> $ARQ_TMP
    fi

    if [ "$ESP" == "TRUE" ]; then
        echo '! @ # $ % ^ & * ( ) _ + - = { } [ ] | ; : < > , . ? /' >> $ARQ_TMP
    fi

    CARAC=$(tr -d ' \n' < $ARQ_TMP)

    for i in {1..3}; do
        SENHA=$(echo "$CARAC" | fold -w1 | shuf | head -n $TAM | tr -d '\n')
        echo "Senha $i: $SENHA"
    done | yad --text-info --width=400 --height=300 --title="Senhas Geradas"
}

RES=$(yad --form \
    --title="Gerador de Senhas" \
    --field="Tamanho da Senha:NUM" \
    --field="Letras Maiúsculas:CHK" \
    --field="Letras Minúsculas:CHK" \
    --field="Números:CHK" \
    --field="Caracteres Especiais:CHK" \
    --button="Gerar Senha":0)

TAM=$(echo $RES | awk -F'|' '{print $1}')
MAI=$(echo $RES | awk -F'|' '{print $2}')
MIN=$(echo $RES | awk -F'|' '{print $3}')
NUM=$(echo $RES | awk -F'|' '{print $4}')
ESP=$(echo $RES | awk -F'|' '{print $5}')

if ! [[ $TAM =~ ^[0-9]+$ ]] || [ $TAM -le 0 ]; then
    yad --error --text="Tamanho da senha inválido!"
    exit 1
fi

gerar "$TAM" "$MAI" "$MIN" "$NUM" "$ESP"