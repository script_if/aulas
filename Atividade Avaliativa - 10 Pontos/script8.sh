#!/bin/bash

for arquivo in *; do
    if [ -f "$arquivo" ]; then
        linhas=$(wc -l < "$arquivo")
        echo "$arquivo: $linhas linhas"
    fi
done
