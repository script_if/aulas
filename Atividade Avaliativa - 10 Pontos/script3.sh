#!/bin/bash

if [ $# -eq 0 ]; then
    echo "Uso: $0 nome_do_arquivo"
    exit 1
fi

arquivo="$1"

if [ -f "$arquivo" ] && [ "$(wc -l < "$arquivo")" -gt 3 ]; then
    echo "GOOD"
else
    echo "O arquivo não existe ou não possui mais de 3 linhas."
fi
