#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Nenhum arquivo fornecido."
    exit 1
fi

for arquivo in "$@"; do
    if [ -f "$arquivo" ]; then
        echo "$arquivo: SIM"
    else
        echo "$arquivo: NAO"
    fi
done
