#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Uso: $0 <a> <b>"
    exit 1
fi

a=$1
b=$2

if ! [[ "$a" =~ ^-?[0-9]+$ ]] || ! [[ "$b" =~ ^-?[0-9]+$ ]] || [ "$a" -gt "$b" ]; then
    echo "Os parâmetros devem ser dois números inteiros e 'a' deve ser menor ou igual a 'b'."
    exit 1
fi

for ((i=a; i<=b; i++)); do
    if [ $((i % 2)) -eq 0 ]; then
        echo "$i"
    fi
done
