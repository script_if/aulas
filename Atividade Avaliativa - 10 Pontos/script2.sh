#!/bin/bash

read -p "Digite o nome do primeiro arquivo: " arquivo1
read -p "Digite o nome do segundo arquivo: " arquivo2
read -p "Digite o nome do terceiro arquivo: " arquivo3
read -p "Digite o nome do quarto arquivo: " arquivo4

contar_linhas() {
    wc -l < "$1" 2>/dev/null
}

for arquivo in "$arquivo1" "$arquivo2" "$arquivo3" "$arquivo4"; do
    if [ ! -f "$arquivo" ]; then
        echo "Algum arquivo não foi encontrado ou não é um arquivo regular. Encerrando."
        exit 1
    fi
done

maior_arquivo=""
max_linhas=0

for arquivo in "$arquivo1" "$arquivo2" "$arquivo3" "$arquivo4"; do
    linhas=$(contar_linhas "$arquivo")
    if [ "$linhas" -gt "$max_linhas" ]; then
        max_linhas="$linhas"
        maior_arquivo="$arquivo"
    fi
done

echo "O arquivo com o maior número de linhas é: $maior_arquivo com $max_linhas linhas."
echo "Lembre-se: nem tudo que é grande é melhor, mas um bom arquivo é sempre valioso!"
