#!/bin/bash

for ((i=1; i<=101; i++)); do
    echo $((RANDOM)) >> num.txt
done

linhas=$(wc -l < num.txt)
if [ "$linhas" -eq 101 ]; then
    echo "O arquivo num.txt tem 101 linhas."
else
    echo "Erro: O arquivo num.txt não tem 101 linhas. Tem $linhas linhas."
    exit 1
fi

soma=0
while read -r numero; do
    soma=$((soma + numero))
done < num.txt

echo "A soma dos números em num.txt é: $soma"
