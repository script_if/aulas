#!/bin/bash

echo "Redirecionadores em Shell Script:"
echo

echo "1. Redirecionador '>':"
echo "   Redireciona a saída padrão (stdout) para um arquivo. Se o arquivo existir, seu conteúdo será substituído."
echo "   Exemplo: echo 'Olá, mundo!' > arquivo.txt"
echo

echo "2. Redirecionador '>>':"
echo "   Redireciona a saída padrão (stdout) para um arquivo, mas adiciona ao final do arquivo se ele já existir."
echo "   Exemplo: echo 'Nova linha' >> arquivo.txt"
echo

echo "3. Redirecionador '2>':"
echo "   Redireciona a saída de erro padrão (stderr) para um arquivo. Se o arquivo existir, seu conteúdo será substituído."
echo "   Exemplo: ls /diretorio_inexistente 2> erro.log"
echo

echo "4. Redirecionador '2>>':"
echo "   Redireciona a saída de erro padrão (stderr) para um arquivo, adicionando ao final do arquivo se ele já existir."
echo "   Exemplo: ls /diretorio_inexistente 2>> erro.log"
echo

echo "5. Redirecionador '&>':"
echo "   Redireciona tanto a saída padrão (stdout) quanto a saída de erro padrão (stderr) para um arquivo, substituindo seu conteúdo."
echo "   Exemplo: comando &> saida_e_erro.log"
echo

echo "6. Redirecionador '&>>':"
echo "   Redireciona tanto a saída padrão (stdout) quanto a saída de erro padrão (stderr) para um arquivo, adicionando ao final do arquivo se ele já existir."
echo "   Exemplo: comando &>> saida_e_erro.log"
echo

echo "7. Redirecionador '<':"
echo "   Redireciona a entrada padrão (stdin) de um arquivo para um comando."
echo "   Exemplo: sort < arquivo.txt"
echo

echo "8. Redirecionador '<<':"
echo "   Redireciona a entrada padrão (stdin) a partir de um texto delimitado."
echo "   Exemplo: cat <<EOF\nTexto aqui\nEOF"
echo

echo "9. Redirecionador '<<<':"
echo "   Redireciona uma string literal para a entrada padrão (stdin) de um comando."
echo "   Exemplo: grep 'texto' <<< 'Esta é a linha contendo texto'"
echo

echo "10. Redirecionador '|':"
echo "    Usa a saída padrão (stdout) de um comando como entrada padrão (stdin) para outro comando."
echo "    Exemplo: ls | grep 'texto'"
echo
